new Vue({
    el: "#app",
    data() {
      return {
        audio: null,
        circleLeft: null,
        barWidth: null,
        duration: null,
        currentTime: null,
        isTimerPlaying: false,
        tracks: [
          {
            name: "Tujh Mein Rab Dikhta Hai",
            artist: "Rab Ne Bana Di Jodi",
            cover: "https://i.pinimg.com/originals/5b/e0/18/5be018e0a937efe50f71eb9efad01d46.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Hindi/1.mp3",
            url: "https://www.youtube.com/watch?v=qoq8B8ThgEM",
            favorited: false
   
          },
          {
            name: "Tu bhoola jise",
            artist: "Airlift",
            cover: "https://is1-ssl.mzstatic.com/image/thumb/Music18/v4/bd/0f/50/bd0f5065-7298-a57b-6069-243109417798/source/1200x1200bb.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Hindi/2.mp3",
            url: "https://www.youtube.com/watch?v=fiJWyk_a5HI",
            favorited: true
          },
          {
            name: "Subhanallah",
            artist: "Yeh jawani diwani",
            cover: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/02fd8ea6-8378-47f5-9c2b-f33adf907fb1/dbt17wd-e47329b2-057c-47ba-80b5-72011fca0aed.jpg/v1/fill/w_894,h_894,q_70,strp/yeh_jawaani_hai_deewani_albumart_by_raviteja16_dbt17wd-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTAyNCIsInBhdGgiOiJcL2ZcLzAyZmQ4ZWE2LTgzNzgtNDdmNS05YzJiLWYzM2FkZjkwN2ZiMVwvZGJ0MTd3ZC1lNDczMjliMi0wNTdjLTQ3YmEtODBiNS03MjAxMWZjYTBhZWQuanBnIiwid2lkdGgiOiI8PTEwMjQifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.99538V027M2tch1TN0qRijMevWlncNV2TVkha527Mb8",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Hindi/3.mp3",
            url: "https://www.youtube.com/watch?v=QYO6AlxiRE4",
            favorited: false
          },
          {
            name: "Nashe si chadh gayi",
            artist: "Befikre",
            cover: "https://i.pinimg.com/originals/1b/81/b3/1b81b33ab73ac4c5c49a343c55bd6ac3.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Hindi/4.mp3",
            url: "https://www.youtube.com/watch?v=Wd2B8OAotU8",
            favorited: false
          }
          
        ],
        currentTrack: null,
        currentTrackIndex: 0,
        transitionName: null
      };
    },
    methods: {
      play() {
        if (this.audio.paused) {
          this.audio.play();
          this.isTimerPlaying = true;
        } else {
          this.audio.pause();
          this.isTimerPlaying = false;
        }
      },
      generateTime() {
        let width = (100 / this.audio.duration) * this.audio.currentTime;
        this.barWidth = width + "%";
        this.circleLeft = width + "%";
        let durmin = Math.floor(this.audio.duration / 60);
        let dursec = Math.floor(this.audio.duration - durmin * 60);
        let curmin = Math.floor(this.audio.currentTime / 60);
        let cursec = Math.floor(this.audio.currentTime - curmin * 60);
        if (durmin < 10) {
          durmin = "0" + durmin;
        }
        if (dursec < 10) {
          dursec = "0" + dursec;
        }
        if (curmin < 10) {
          curmin = "0" + curmin;
        }
        if (cursec < 10) {
          cursec = "0" + cursec;
        }
        this.duration = durmin + ":" + dursec;
        this.currentTime = curmin + ":" + cursec;
      },
      updateBar(x) {
        let progress = this.$refs.progress;
        let maxduration = this.audio.duration;
        let position = x - progress.offsetLeft;
        let percentage = (100 * position) / progress.offsetWidth;
        if (percentage > 100) {
          percentage = 100;
        }
        if (percentage < 0) {
          percentage = 0;
        }
        this.barWidth = percentage + "%";
        this.circleLeft = percentage + "%";
        this.audio.currentTime = (maxduration * percentage) / 100;
        this.audio.play();
      },
      clickProgress(e) {
        this.isTimerPlaying = true;
        this.audio.pause();
        this.updateBar(e.pageX);
      },
      prevTrack() {
        this.transitionName = "scale-in";
        this.isShowCover = false;
        if (this.currentTrackIndex > 0) {
          this.currentTrackIndex--;
        } else {
          this.currentTrackIndex = this.tracks.length - 1;
        }
        this.currentTrack = this.tracks[this.currentTrackIndex];
        this.resetPlayer();
      },
      nextTrack() {
        this.transitionName = "scale-out";
        this.isShowCover = false;
        if (this.currentTrackIndex < this.tracks.length - 1) {
          this.currentTrackIndex++;
        } else {
          this.currentTrackIndex = 0;
        }
        this.currentTrack = this.tracks[this.currentTrackIndex];
        this.resetPlayer();
      },
      resetPlayer() {
        this.barWidth = 0;
        this.circleLeft = 0;
        this.audio.currentTime = 0;
        this.audio.src = this.currentTrack.source;
        setTimeout(() => {
          if(this.isTimerPlaying) {
            this.audio.play();
          } else {
            this.audio.pause();
          }
        }, 300);
      },
      favorite() {
        this.tracks[this.currentTrackIndex].favorited = !this.tracks[
          this.currentTrackIndex
        ].favorited;
      }
    },
    created() {
      let vm = this;
      this.currentTrack = this.tracks[0];
      this.audio = new Audio();
      this.audio.src = this.currentTrack.source;
      this.audio.ontimeupdate = function() {
        vm.generateTime();
      };
      this.audio.onloadedmetadata = function() {
        vm.generateTime();
      };
      this.audio.onended = function() {
        vm.nextTrack();
        this.isTimerPlaying = true;
      };
  
      // this is optional (for preload covers)
      for (let index = 0; index < this.tracks.length; index++) {
        const element = this.tracks[index];
        let link = document.createElement('link');
        link.rel = "prefetch";
        link.href = element.cover;
        link.as = "image"
        document.head.appendChild(link)
      }
    }
  });
  