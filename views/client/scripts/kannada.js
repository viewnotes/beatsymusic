new Vue({
    el: "#app",
    data() {
      return {
        audio: null,
        circleLeft: null,
        barWidth: null,
        duration: null,
        currentTime: null,
        isTimerPlaying: false,
        tracks: [
          {
            name: "Belageddu",
            artist: "Kirik Party ",
            cover: "https://4.bp.blogspot.com/-hO5Yp1GpriQ/W6tcfRCibHI/AAAAAAAAFMU/kuY6d8IO_owNqDMP4ak_q_SvN9CMOM57wCLcBGAs/s1600/1.gif",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/kannada/1.mp3",
            url: "https://www.youtube.com/watch?v=ebz20FHrT44&list=RDXv1NQFN9Kd8&index=2",
            favorited: false
          },
          {
            name: "Ondu Malebillu",
            artist: "Chakravarthy",
            cover: "https://extraimage.net/images/2017/09/28/948d361692e45fae66055c5a3326a9c6.md.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/kannada/2.mp3",
            url: "https://www.youtube.com/watch?v=Xv1NQFN9Kd8",
            favorited: true
          },
          {
            name: "BOMBE HELUTAITHE",
            artist: "RAAJAKUMARA",
            cover: "https://data1.ibtimes.co.in/cache-img-600-0-photo/en/full/58418/1487759628_raajakumara-upcoming-kannada-action-movie-written-directed-by-santhosh-ananddram-produced-by.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/kannada/3.mp3",
            url: "https://www.youtube.com/watch?v=gy5_T2ACerk&list=RDXv1NQFN9Kd8&index=5",
            favorited: false
          },
          {
            name: "Raja Raniyanthe ",
            artist: "Rhaatee",
            cover: "https://c.saavncdn.com/171/Rhaatee-Kannada-2015-500x500.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/kannada/4.mp3",
            url: "https://www.youtube.com/watch?v=8tyqLHyx0PI",
            favorited: false
          },
          {
            name: "Marethuhoyithe",
            artist: "Amar",
            cover: "https://i.ytimg.com/vi/cwEPvbYIT4M/maxresdefault.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/kannada/5.mp3",
            url: "https://www.youtube.com/watch?v=cwEPvbYIT4M&list=PL7A9n1TybRSkf6qJWEqU_UpQCskBxNG6H",
            favorited: false
          }
          
        ],
        currentTrack: null,
        currentTrackIndex: 0,
        transitionName: null
      };
    },
    methods: {
      play() {
        if (this.audio.paused) {
          this.audio.play();
          this.isTimerPlaying = true;
        } else {
          this.audio.pause();
          this.isTimerPlaying = false;
        }
      },
      generateTime() {
        let width = (100 / this.audio.duration) * this.audio.currentTime;
        this.barWidth = width + "%";
        this.circleLeft = width + "%";
        let durmin = Math.floor(this.audio.duration / 60);
        let dursec = Math.floor(this.audio.duration - durmin * 60);
        let curmin = Math.floor(this.audio.currentTime / 60);
        let cursec = Math.floor(this.audio.currentTime - curmin * 60);
        if (durmin < 10) {
          durmin = "0" + durmin;
        }
        if (dursec < 10) {
          dursec = "0" + dursec;
        }
        if (curmin < 10) {
          curmin = "0" + curmin;
        }
        if (cursec < 10) {
          cursec = "0" + cursec;
        }
        this.duration = durmin + ":" + dursec;
        this.currentTime = curmin + ":" + cursec;
      },
      updateBar(x) {
        let progress = this.$refs.progress;
        let maxduration = this.audio.duration;
        let position = x - progress.offsetLeft;
        let percentage = (100 * position) / progress.offsetWidth;
        if (percentage > 100) {
          percentage = 100;
        }
        if (percentage < 0) {
          percentage = 0;
        }
        this.barWidth = percentage + "%";
        this.circleLeft = percentage + "%";
        this.audio.currentTime = (maxduration * percentage) / 100;
        this.audio.play();
      },
      clickProgress(e) {
        this.isTimerPlaying = true;
        this.audio.pause();
        this.updateBar(e.pageX);
      },
      prevTrack() {
        this.transitionName = "scale-in";
        this.isShowCover = false;
        if (this.currentTrackIndex > 0) {
          this.currentTrackIndex--;
        } else {
          this.currentTrackIndex = this.tracks.length - 1;
        }
        this.currentTrack = this.tracks[this.currentTrackIndex];
        this.resetPlayer();
      },
      nextTrack() {
        this.transitionName = "scale-out";
        this.isShowCover = false;
        if (this.currentTrackIndex < this.tracks.length - 1) {
          this.currentTrackIndex++;
        } else {
          this.currentTrackIndex = 0;
        }
        this.currentTrack = this.tracks[this.currentTrackIndex];
        this.resetPlayer();
      },
      resetPlayer() {
        this.barWidth = 0;
        this.circleLeft = 0;
        this.audio.currentTime = 0;
        this.audio.src = this.currentTrack.source;
        setTimeout(() => {
          if(this.isTimerPlaying) {
            this.audio.play();
          } else {
            this.audio.pause();
          }
        }, 300);
      },
      favorite() {
        this.tracks[this.currentTrackIndex].favorited = !this.tracks[
          this.currentTrackIndex
        ].favorited;
      }
    },
    created() {
      let vm = this;
      this.currentTrack = this.tracks[0];
      this.audio = new Audio();
      this.audio.src = this.currentTrack.source;
      this.audio.ontimeupdate = function() {
        vm.generateTime();
      };
      this.audio.onloadedmetadata = function() {
        vm.generateTime();
      };
      this.audio.onended = function() {
        vm.nextTrack();
        this.isTimerPlaying = true;
      };
  
      // this is optional (for preload covers)
      for (let index = 0; index < this.tracks.length; index++) {
        const element = this.tracks[index];
        let link = document.createElement('link');
        link.rel = "prefetch";
        link.href = element.cover;
        link.as = "image"
        document.head.appendChild(link)
      }
    }
  });
  