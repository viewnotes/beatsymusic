new Vue({
    el: "#app",
    data() {
      return {
        audio: null,
        circleLeft: null,
        barWidth: null,
        duration: null,
        currentTime: null,
        isTimerPlaying: false,
        tracks: [
          {
            name: "Madhu pole",
            artist: "Dear comrade",
            cover: "https://timesofindia.indiatimes.com/thumb/msid-69282733,width-800,height-600,resizemode-4/69282733.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Malayalam/1.mp3",
            url: "https://www.youtube.com/watch?v=0ybAHLO5taM",
            favorited: false
          },
          {
            name: "Ni himamazhayay varu",
            artist: "Edakkad battalion 06",
            cover: "https://timesofindia.indiatimes.com/thumb/msid-71181926,width-800,height-600,resizemode-4/71181926.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Malayalam/2.mp3",
            url: "https://www.youtube.com/watch?v=nKEVIuya2kY",
            favorited: true
          },
          {
            name: "Etho mazhayil",
            artist: "Vijay superum pournamiyum",
            cover: "https://m.media-amazon.com/images/I/716VpreDr8L._SS500_.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Malayalam/3.mp3",
            url: "https://www.youtube.com/watch?v=kwQCvJE4GjI",
            favorited: false
          },
          {
            name: "Mazhaye thoomazhaye ",
            artist: "Pattam pole",
            cover: "https://alchetron.com/cdn/Pattam-Pole-images-a07fa103-e3b6-47db-84e8-d220268816c.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Malayalam/4.mp3",
            url: "https://www.youtube.com/watch?v=ZJyFwAs2HUI",
            favorited: false
          },
          {
            name: "Onnum Mindathe",
            artist: "Kodathi Samaksham Balan Vakkeel",
            cover: "https://m.timesofindia.com/thumb/msid-67975112,imgsize-442478,width-800,height-600,resizemode-4/67975112.jpg",
            source: "https://raw.githubusercontent.com/ajshah7/music/master/music/Malayalam/5.mp3",
            url: "https://www.youtube.com/watch?v=Br1ednUJSuU",
            favorited: false
          }
          
        ],
        currentTrack: null,
        currentTrackIndex: 0,
        transitionName: null
      };
    },
    methods: {
      play() {
        if (this.audio.paused) {
          this.audio.play();
          this.isTimerPlaying = true;
        } else {
          this.audio.pause();
          this.isTimerPlaying = false;
        }
      },
      generateTime() {
        let width = (100 / this.audio.duration) * this.audio.currentTime;
        this.barWidth = width + "%";
        this.circleLeft = width + "%";
        let durmin = Math.floor(this.audio.duration / 60);
        let dursec = Math.floor(this.audio.duration - durmin * 60);
        let curmin = Math.floor(this.audio.currentTime / 60);
        let cursec = Math.floor(this.audio.currentTime - curmin * 60);
        if (durmin < 10) {
          durmin = "0" + durmin;
        }
        if (dursec < 10) {
          dursec = "0" + dursec;
        }
        if (curmin < 10) {
          curmin = "0" + curmin;
        }
        if (cursec < 10) {
          cursec = "0" + cursec;
        }
        this.duration = durmin + ":" + dursec;
        this.currentTime = curmin + ":" + cursec;
      },
      updateBar(x) {
        let progress = this.$refs.progress;
        let maxduration = this.audio.duration;
        let position = x - progress.offsetLeft;
        let percentage = (100 * position) / progress.offsetWidth;
        if (percentage > 100) {
          percentage = 100;
        }
        if (percentage < 0) {
          percentage = 0;
        }
        this.barWidth = percentage + "%";
        this.circleLeft = percentage + "%";
        this.audio.currentTime = (maxduration * percentage) / 100;
        this.audio.play();
      },
      clickProgress(e) {
        this.isTimerPlaying = true;
        this.audio.pause();
        this.updateBar(e.pageX);
      },
      prevTrack() {
        this.transitionName = "scale-in";
        this.isShowCover = false;
        if (this.currentTrackIndex > 0) {
          this.currentTrackIndex--;
        } else {
          this.currentTrackIndex = this.tracks.length - 1;
        }
        this.currentTrack = this.tracks[this.currentTrackIndex];
        this.resetPlayer();
      },
      nextTrack() {
        this.transitionName = "scale-out";
        this.isShowCover = false;
        if (this.currentTrackIndex < this.tracks.length - 1) {
          this.currentTrackIndex++;
        } else {
          this.currentTrackIndex = 0;
        }
        this.currentTrack = this.tracks[this.currentTrackIndex];
        this.resetPlayer();
      },
      resetPlayer() {
        this.barWidth = 0;
        this.circleLeft = 0;
        this.audio.currentTime = 0;
        this.audio.src = this.currentTrack.source;
        setTimeout(() => {
          if(this.isTimerPlaying) {
            this.audio.play();
          } else {
            this.audio.pause();
          }
        }, 300);
      },
      favorite() {
        this.tracks[this.currentTrackIndex].favorited = !this.tracks[
          this.currentTrackIndex
        ].favorited;
      }
    },
    created() {
      let vm = this;
      this.currentTrack = this.tracks[0];
      this.audio = new Audio();
      this.audio.src = this.currentTrack.source;
      this.audio.ontimeupdate = function() {
        vm.generateTime();
      };
      this.audio.onloadedmetadata = function() {
        vm.generateTime();
      };
      this.audio.onended = function() {
        vm.nextTrack();
        this.isTimerPlaying = true;
      };
  
      // this is optional (for preload covers)
      for (let index = 0; index < this.tracks.length; index++) {
        const element = this.tracks[index];
        let link = document.createElement('link');
        link.rel = "prefetch";
        link.href = element.cover;
        link.as = "image"
        document.head.appendChild(link)
      }
    }
  });
  