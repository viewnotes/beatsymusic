

new Vue({
  
  el: "#app",
  
  data() {
    
    return {
      
      audio: null,
      circleLeft: null,
      barWidth: null,
      duration: null,
      currentTime: null,
      isTimerPlaying: false,
      tracks: [
        {
          name: "Lose you to love me",
          artist: "Selena Gomez",
          cover: "https://images.genius.com/6e4e0c564cbcf564f38c829d6629e286.1000x1000x1.jpg",
          source: "https://raw.githubusercontent.com/ajshah7/music/master/music/English/1.mp3",
          url: "https://www.youtube.com/watch?v=zlJDTxahav0",
          favorited: false
        },
        {
          name: "Memories",
          artist: "Maroon 5",
          cover: "https://i.ytimg.com/vi/qZjYggYSzc8/maxresdefault.jpg",
          source: "https://raw.githubusercontent.com/ajshah7/music/master/music/English/2.mp3",
          url: "https://www.youtube.com/watch?v=SlPhMPnQ58k",
          favorited: true
        },
        {
          name: "Senorita",
          artist: "shawn mendes & camila cabello",
          cover: "https://thedigitalwise.com/wp-content/uploads/2019/06/D9hf8ADXUAEGH_R.jpg",
          source: "https://raw.githubusercontent.com/ajshah7/music/master/music/English/3.mp3",
          url: "https://www.youtube.com/watch?v=xq866Q7GUlc",
          favorited: false
        },
        {
          name: "Bounce ",
          artist: "Maxwell Aden ft. Kodie Shane",
          cover: "https://i.ytimg.com/vi/qCA35EupxHg/maxresdefault.jpg",
          source: "https://raw.githubusercontent.com/ajshah7/music/master/music/English/4.mp3",
          url: "https://www.youtube.com/watch?v=qCA35EupxHg",
          favorited: false
        },
        {
          name: "Look at her now cover art",
          artist: "Selena Gomez",
          cover: "https://i1.wp.com/udou.ph/wp-content/uploads/2019/10/look-at-her-now-selena-gomez-song-udou-header.jpg?fit=1200%2C627&ssl=1",
          source: "https://raw.githubusercontent.com/ajshah7/music/master/music/English/5.mp3",
          url: "https://www.youtube.com/watch?v=8u-_64S7plI",
          favorited: true
        },
        {
          name: "Harleys in Hawaii",
          artist: "Katy Perry",
          cover: "https://s3.amazonaws.com/media.thecrimson.com/photos/2019/10/28/233029_1340380.jpg",
          source: "https://raw.githubusercontent.com/ajshah7/music/master/music/English/6.mp3",
          url: "https://www.youtube.com/watch?v=sQEgklEwhSo",
          favorited: false
        },
        {
          name: "A whole new world cover art",
          artist: "Zayn Malik & Zhavia Ward",
          cover: "https://i.ytimg.com/vi/_THdjo0ueac/hqdefault.jpg",
          source: "https://raw.githubusercontent.com/ajshah7/music/master/music/English/7.mp3",
          url: "https://www.youtube.com/watch?v=rg_zwK_sSEY",
          favorited: true
        },
        {
          name: "Overdose",
          artist: "Grandson",
          cover: "https://raw.githubusercontent.com/muhammederdem/mini-player/master/img/8.jpg",
          source: "https://raw.githubusercontent.com/muhammederdem/mini-player/master/mp3/8.mp3",
          url: "https://www.youtube.com/watch?v=00-Rl3Jlx-o",
          favorited: false
        },
        {
          name: "Rag'n'Bone Man",
          artist: "Human",
          cover: "https://raw.githubusercontent.com/muhammederdem/mini-player/master/img/9.jpg",
          source: "https://raw.githubusercontent.com/muhammederdem/mini-player/master/mp3/9.mp3",
          url: "https://www.youtube.com/watch?v=L3wKzyIN1yk",
          favorited: false
        }
      ],
      currentTrack: null,
      currentTrackIndex: 0,
      transitionName: null
    };
  },
  methods: {
    play() {
      if (this.audio.paused) {
        this.audio.play();
        this.isTimerPlaying = true;
      } else {
        this.audio.pause();
        this.isTimerPlaying = false;
      }
    },
    generateTime() {
      let width = (100 / this.audio.duration) * this.audio.currentTime;
      this.barWidth = width + "%";
      this.circleLeft = width + "%";
      let durmin = Math.floor(this.audio.duration / 60);
      let dursec = Math.floor(this.audio.duration - durmin * 60);
      let curmin = Math.floor(this.audio.currentTime / 60);
      let cursec = Math.floor(this.audio.currentTime - curmin * 60);
      if (durmin < 10) {
        durmin = "0" + durmin;
      }
      if (dursec < 10) {
        dursec = "0" + dursec;
      }
      if (curmin < 10) {
        curmin = "0" + curmin;
      }
      if (cursec < 10) {
        cursec = "0" + cursec;
      }
      this.duration = durmin + ":" + dursec;
      this.currentTime = curmin + ":" + cursec;
    },
    updateBar(x) {
      let progress = this.$refs.progress;
      let maxduration = this.audio.duration;
      let position = x - progress.offsetLeft;
      let percentage = (100 * position) / progress.offsetWidth;
      if (percentage > 100) {
        percentage = 100;
      }
      if (percentage < 0) {
        percentage = 0;
      }
      this.barWidth = percentage + "%";
      this.circleLeft = percentage + "%";
      this.audio.currentTime = (maxduration * percentage) / 100;
      this.audio.play();
    },
    clickProgress(e) {
      this.isTimerPlaying = true;
      this.audio.pause();
      this.updateBar(e.pageX);
    },
    prevTrack() {
      this.transitionName = "scale-in";
      this.isShowCover = false;
      if (this.currentTrackIndex > 0) {
        this.currentTrackIndex--;
      } else {
        this.currentTrackIndex = this.tracks.length - 1;
      }
      this.currentTrack = this.tracks[this.currentTrackIndex];
      this.resetPlayer();
    },
    nextTrack() {
      this.transitionName = "scale-out";
      this.isShowCover = false;
      if (this.currentTrackIndex < this.tracks.length - 1) {
        this.currentTrackIndex++;
      } else {
        this.currentTrackIndex = 0;
      }
      this.currentTrack = this.tracks[this.currentTrackIndex];
      this.resetPlayer();
    },
    resetPlayer() {
      this.barWidth = 0;
      this.circleLeft = 0;
      this.audio.currentTime = 0;
      this.audio.src = this.currentTrack.source;
      setTimeout(() => {
        if(this.isTimerPlaying) {
          this.audio.play();
        } else {
          this.audio.pause();
        }
      }, 300);
    },
    favorite() {
      this.tracks[this.currentTrackIndex].favorited = !this.tracks[
        this.currentTrackIndex
      ].favorited;
    }
  },
  created() {
    let vm = this;
    this.currentTrack = this.tracks[0];
    this.audio = new Audio();
    this.audio.src = this.currentTrack.source;
    this.audio.ontimeupdate = function() {
      vm.generateTime();
    };
    this.audio.onloadedmetadata = function() {
      vm.generateTime();
    };
    this.audio.onended = function() {
      vm.nextTrack();
      this.isTimerPlaying = true;
    };

    // this is optional (for preload covers)
    for (let index = 0; index < this.tracks.length; index++) {
      const element = this.tracks[index];
      let link = document.createElement('link');
      link.rel = "prefetch";
      link.href = element.cover;
      link.as = "image"
      document.head.appendChild(link)
    }
  }
});
